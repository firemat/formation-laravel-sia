# Dockerfile
FROM php:7.4-fpm

RUN apt-get update -y && apt-get install -y libmcrypt-dev

# Copy composer.json
COPY composer.json /var/www/

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    libzip-dev \
    locales \
    zip \
    unzip \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_pgsql zip pcntl bcmath

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# install xdebug
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

# Install PHP Xdebug configuration
ADD ./docker-config/xdebug.ini /etc/php/conf.d/
ADD ./docker-config/xdebug.ini /usr/local/etc/php/conf.d/
RUN mkdir /var/log/xdebug
RUN cat > /var/log/xdebug/xdebug.log

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
# COPY --chown=www:www . /var/www
RUN chown -R $USER:www-data storage bootstrap/cache /var/log/xdebug
RUN chmod -R 777 /var/log/xdebug
RUN chmod -R 775 storage bootstrap/cache

# Change current user to www
USER www

# Expose port 9000(fpm)/9001(xDebug) and start php-fpm server
EXPOSE 9000
EXPOSE 9001
CMD ["php-fpm"]
