[![Licence GPL](http://img.shields.io/badge/license-GPL-green.svg)](http://www.gnu.org/licenses/quick-guide-gplv3.fr.html)

# Formation SIA Laravel

Correction of the exercice proposed for the SIA formation on Laravel

## Troubleshooting

To report a bug, or a dysfunction feel free to submit issues

## Deployment

This app can be deployed with `docker-compose`, just use `docker-compose up -d` :)

If you already have `composer` and `php` on your system, just use `composer first-install`, else you can check the detailed method from the wiki :)

Make sure that the ports `80`, `443` and `5432` are available on your computer. Help for deployment is provided on the wiki.

## Available Composer commands

### ``composer clean-laravel``
Clean the cached files created by Laravel

### ``composer phpcs``
Run the project linter on the app code

### ``composer phpDoc``
Generate the documentation of the project (needs to have [phpDocumentor](https://github.com/phpdocumentor/phpdocumentor) installed using [phive](https://phar.io/))

### ``composer test``
Run the migration and the test on the App docker container

### ``composer docker-build``
> **Warning: You should remove, or add proper permission, to the `docker-db` folder (if existing)**

Down every app related container, build the docker image if necessary and launch the app

### ``composer docker-up``
Launch the app and the associated containers

### ``composer docker-reset``
Down every app related container and launch the app

### ``composer docker-down``
Down every app related container

### ``composer first-install``
> Only for people that already have composer and php globally accessible

Create the `.env` file, launch the containers, install dependencies, regenerate app key, and finally, run migration and test.

## Built With

- [Laravel](https://github.com/laravel/laravel) &mdash; Our back-end is a Laravel app using PHP 7.4.
- [PostgreSQL](http://www.postgresql.org/) &mdash; Our main data store is in Postgres.
- [Nginx](https://nginx.org/) &mdash; Our HTTP Server to serve our Laravel app.

And lots of other dependencies that you can find in [Composer](/composer.json) file.

## Contributions

After checking [GitLab Issues](https://gitlab.com/firemat/formation-laravel-sia/issues),
feel free to contribute by sending your pull requests.
While we may not merge your PR as is, they serve to start conversations 
and improve the general experience for all users.

## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
Formation Laravel SIA
Copyright (C) 2020 Halunka Matthieu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
