<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
* Classe permettant de remplir la base de données utilisateur.
*
* @package  Database\seeds
* @author   Matthieu Halunka <matthieu.halunka@laposte.net>
* @version  $Revision: 1.0 $
* @access   public
*/
class UserTableSeeder extends Seeder {
    /**
    * Méthode permettant de remplir la table users et users_entreprise.
    */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'nom' => 'admin',
            'prenom' => 'dupont',
            'pseudo' => 'UltimateCatlover',
            'age' => 18,
            'email' => 'admin@blop.fr',
            'password' => Hash::make('password'),
            'admin' => 1,
        ]);

        for($i = 0; $i < 2; ++$i)
        {
            DB::table('users')->insert([
                'nom' => 'kevin - ' . $i,
                'prenom' => 'dupont',
                'pseudo' => 'Catlover' . $i,
                'age' => 10*$i,
                'email' => 'email' . $i . '@blop.fr',
                'password' => Hash::make('password' . $i),
                'admin' => 0,
            ]);
        }

    }
}
