<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
/**
 * Classe permettant de remplir la base de données laravel.
 *
 * @package  Database\Seeders
 * @author   Matthieu Halunka <matthieu.halunka@laposte.net>
 * @version  $Revision: 1.0 $
 * @access   public
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
    }
}
