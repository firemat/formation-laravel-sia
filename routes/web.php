<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', [Controller::class, 'home']);

Route::get('/', [Controller::class, 'home'])->name('accueil');

Route::prefix("solution")->group(function () {
    Route::get('/', [Controller::class, 'index'])->name('index');
    Route::get('userspace', [Controller::class, 'getDashboardUser'])->name('user');
    Route::get('adminspace', [Controller::class, 'getDashboardAdmin'])->name('admin');
    Route::post('adminspace/delete', [Controller::class, 'deleteUser'])->name('suppression');

    Route::post('signUp', [Controller::class, 'addUser'])->name('inscription');
    Route::post('logIn', [Controller::class, 'connexion'])->name('connexion');
    Route::post('logOut', [Controller::class, 'disconnect'])->name('deconnexion');
});
