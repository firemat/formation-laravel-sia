<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

/**
 * Classe en charge des propriétés de l'objet métier Utilisateur
 *
 * @package  App
 * @author   Matthieu Halunka <matthieu.halunka@laposte.net>
 * @version  Revision: 1.0
 * @access   public
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * La table associée au model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom','pseudo','age','email','password', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','admin'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Override pour éviter d'encrypter explicitement le mot de passe à chaque affectation.
     * @param string $password Mot de passe à encrypter et à affecter.
     */
    public function setPasswordAttribute(string $password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Renvoie un entier 0 ou 1 représentant le statut administrateur de l'utilisateur.
     * @return      int      0 si n'est pas administrateur, 1 sinon.
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * Get the column name for the "remember me" session.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return '';
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // do nothing
    }
}
