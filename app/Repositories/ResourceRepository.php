<?php
/**
* Classe abstraite contenant les méthodes communes aux objets utilisant Eloquent.
*/
namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
* Classe abstraite contenant les méthodes communes aux objets utilisant Eloquent.
*
* @package  App\Repositories
* @author   Matthieu Halunka <matthieu.halunka@laposte.net>
* @version  Revision: 1.0
* @access   public
*/
abstract class ResourceRepository
{

    /**
    * Modèle vierge pour modifier la base associée à ce dernier.
    *
    * @var Eloquent model
    */
    protected $model;

    /**
     * Renvoie le modèle par tranche de n enregistrement
     * @param  int          $n          Entier correspondant au nombre d'enregistrement à récupérer
     * @return Model|Collection L'ensemble des modèles trouvées
     */
    public function getPaginate($n)
    {
        return $this->model->paginate($n);
    }

    /**
     * Stocke dans la base le modèle
     * @param  array      $inputs     Ensemble des attributs nécessaire à la création du modèle.
     * @return Model                  Le modèle crée
     */
    public function store(Array $inputs)
    {
        return $this->model->firstOrCreate($inputs);
    }

    /**
     * Renvoie le modèle correspondant à l'id donnée
     * @param  array|int        $id         Ensemble des ids pour lesquels on souhaite obtenir les modèles.
     * @return Model|Collection L'ensemble des modèles trouvées
     *@throws ModelNotFoundException si le model est introuvable
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Met à jour le modèle correspondant à l'id donné
     * @param int $id Id du modèle à mettre à jour.
     * @param array $inputs Ensemble des attributs à modifier.
     * @return int      Nombre de tuples modifiés
     */
    public function update(int $id, array $inputs)
    {
        return $this->getById($id)->update($inputs);
    }

    /**
     * Supprime le modèle correspondant à l'id donné
     * @param int $id Id du modèle à supprimer.
     * @return bool|null            Réussite de la suppression.
     * @throws Exception Si la suppression rencontre un problème.
     */
    public function destroy(int $id)
    {
        return $this->getById($id)->delete();
    }
}
