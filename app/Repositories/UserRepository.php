<?php
/**
 * Classe en charge de la manipulation des utilisateurs au niveau du modèle Eloquent et des tables associées.
 */
namespace App\Repositories;

use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Classe en charge de la manipulation des utilisateurs au niveau du modèle Eloquent et des tables associées.
 *
 * @package  App\Repositories
 * @author   Matthieu Halunka <matthieu.halunka@laposte.net>
 * @version  Revision: 1.0
 * @access   public
 */
class UserRepository extends ResourceRepository
{
    /**
     * Un utilisateur vierge pour réaliser nos requêtes.
     *
     * @var User
     */
    protected $model;

    /**
     * Crée une nouvelle instance de App\Repositories\UserRepository
     *
     * @param User $user     Modèle vierge à utiliser.
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * Renvoie un utilisateur correspondant à l'adresse mail renseignée
     * @param string $email Adresse mail de l'utilisateur recherché
     * @return Model|null l'utilisateur trouvé
     */
    public function getByMail(string $email)
    {
        return $this->model->where('email', $email)->first();
    }

    /**
     * Renvoie un utilisateur correspondant au pseudo renseignée
     * @param string $pseudo Pseudo de l'utilisateur recherché
     * @return Model|null l'utilisateur trouvé
     */
    public function getByPseudo(string $pseudo)
    {
        return $this->model->where('pseudo', $pseudo)->first();
    }

    /**
     * Crée l'utilisateur et ses relations au sein de la base.
     * @param array $info L'ensemble des champs requis pour créer un utilisateur
     * @throws Exception si pseudo utilisé ou création impossible
     * @return Model|Collection le nouvel utilisateur
     */
    public function create(Array $info)
    {
        if (!is_null($this->model->where('pseudo', $info['pseudo'])->first())) {
            throw new Exception("Ce login est déjà utilisé !");
        }
        $user=$this->model->firstOrCreate($info);
        if (is_null($user)) {
            throw new Exception("L'insertion a rencontrer un problème !");
        }
        return $user;
    }

    /**
     * Renvoie le nombre d'administrateur dans la base.
     * @return int      Le nombre d'administrateur total.
     */
    public function countAdmin()
    {
        return $this->model->where('admin', 1)->count();
    }

    /**
     * Obtient tout les utilisateurs
     * @throws Exception
     * @return Model|Collection les utilisateurs
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Supprime l'utilisateur et ses relations au sein de la base
     * @param User $user L'utilisateur à supprimer
     * @throws Exception
     * @return boolean|null              Réussite de la mise à jour
     */
    public function delete(User $user)
    {
        return $user->delete();
    }
}
