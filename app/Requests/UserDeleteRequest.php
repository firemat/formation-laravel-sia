<?php
/**
* Requête associée à la création d'un utilisateur.
*/
namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
* Requête associée à la suppressin d'un utilisateur.
*
* @package  App\Requests
* @author   Matthieu Halunka <matthieu.halunka@laposte.net>
* @version  Revision: 1.0
* @access   public
*/
class UserDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['pseudo' => 'required|string|filled'];
    }

     /**
     * Get the validation message that apply to the request in case of validation error.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'pseudo.required' => 'Veuillez indiquer un pseudo.',
            'pseudo.filled' => 'Veuillez indiquer un pseudo.'
        ];
    }
}
