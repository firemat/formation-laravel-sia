<?php
/**
* Requête associée à l'authentification.
*/
namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
* Requête associée à l'authentification.
*
* @package  App\Http\Requests
* @author   Matthieu Halunka <matthieu.halunka@laposte.net>
* @version  Revision: 1.0
* @access   public
*/
class ConnexionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pseudo' => 'bail|required|filled|max:255',
            'password' => 'bail|required|filled'
        ];
    }
}
