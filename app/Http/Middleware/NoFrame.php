<?php
/**
* Middleware ajoutant des headers aux réponses HTTP du serveur dans des buts de sécurité.
*/
namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

/**
* Middleware ajoutant des headers aux réponses HTTP du serveur dans des buts de sécurité.
* @package  App\Http\Middleware
* @author   Matthieu Halunka <matthieu.halunka@laposte.net>
* @version  Revision: 1.0
* @access   public
*/
class NoFrame
{
    /**
     * Ajoute les headers HTTP pour la sécurité des requêtes
     *
     * @param   Request $request
     * @param   Closure $next
     * @return   mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        //On interdit l'utilisation de frame
        $response->header('Content-Security-Policy', "frame-ancestors 'none'");
        $response->header('X-Frame-Options', "DENY");
        //On force la protection XSS
        $response->header('X-XSS-Protection', "1; mode=block");

        //On empêche un client tiers de demander un script css ou js directement
        $response->header('X-Content-Type', "nosniff");
        //On s'assure que le domaine est bien celui du site
        $response->header('Access-Control-Allow-Origin', config('app')['url']);

        return $response;
    }
}
