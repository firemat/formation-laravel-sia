<?php
/**
* Middleware vérifiant que l'utilisateur est bien un administrateur connecté.
*/
namespace App\Http\Middleware;

use Closure;

/**
* Middleware vérifiant que l'utilisateur est bien un administrateur connecté.
* @package  App\Http\Middleware
* @author   Matthieu Halunka <matthieu.halunka@laposte.net>
* @version  Revision: 1.0
* @access   public
*/
class VerifierAdmin
{
    /**
     * Vérifie que l'appel aux méthodes du contrôleur du portail d'administration
     * est bien faite par un administrateur connecté et renvoie vers la page de connexion si ce n'est pas le cas.
     *
     * @param   \Illuminate\Http\Request $request
     * @param   Closure $next
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check()&&auth()->user()->isAdmin()) {
            return $next($request);
        }

        return abort(404);
    }
}
