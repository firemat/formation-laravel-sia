<?php
/**
* Middleware vérifiant que la requête est bien faite par un client connecté.
*/
namespace App\Http\Middleware;

use Closure;

/**
* Middleware vérifiant que la requête est bien faite par un client connecté.
* @package  App\Http\Middleware
* @author   Matthieu Halunka <matthieu.halunka@laposte.net>
* @version  Revision: 1.0
* @access   public
*/
class VerifierClient
{
    /**
     * Vérifie que l'appel aux méthodes du contrôleur du portail pour un client sinon renvoie à l'espace client
     * est bien faite par un administrateur connecté.
     * @param   \Illuminate\Http\Request $request
     * @param   Closure $next
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check()&&!auth()->user()->isAdmin()) {
            return $next($request);
        }

        return abort(404);
    }
}
