<?php

namespace App\Http\Controllers;

use App\Requests\UserDeleteRequest;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Classe en charge des interactions entre l'utilisateur et les pages.
 *
 * @package  App\Http\Controllers
 * @author   Matthieu Halunka <matthieu.halunka@laposte.net>
 * @version  Revision: 1.0
 * @access   public
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Instance du contrôleur en charge des opérations sur les utilisateurs.
     *
     * @var UserRepository
     */
    protected UserRepository $user_repository;

    /**
     * Crée une nouvelle instance de Controller.
     *
     * @param   UserRepository     $user_repository
     */
    public function __construct(UserRepository $user_repository)
    {
        $this->user_repository=$user_repository;
        $this->middleware('checkAdmin')->only('deleteUser', 'getDashboardAdmin');
        $this->middleware('checkClient')->only('getDashboardUser');
    }

    /**
     * Renvoie la page initiale
     * @return Application|Factory|View    Vue contenant la page initiale
     */
    public function home()
    {
        return view('welcome');
    }

    /**
     * Renvoie la page d'accueil
     * @return Application|Factory|View    Vue contenant la page d'accueil
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Enregistre un utilisateur
     * @param Request $request Requete HTTP correspondante au formulaire
     * @return  Response Réponse HTTP avec le json en body
     */
    public function addUser(Request $request)
    {
        try {
            $age=$this->stoi($request->get("age"));
        } catch (Exception $e) {
            if ($request->wantsJson()) {
                return response()->json(array('succes'=>false, 'message'=>"Veuillez saisir un age correct"), 500);
            } else {
                $error="Veuillez saisir un age correct";
                return view('index')->with("error", $error);
            }
        }
        $info_user=[];
        $info_user['pseudo']=$request->nom;
        $info_user['email']=$request->email;
        $info_user['nom']=$request->nom;
        $info_user['prenom']=$request->prenom;
        $info_user['age']=$age;
        $info_user['password']=$request->password;
        try {
            $this->user_repository->create($info_user);
            if ($request->wantsJson()) {
                return response()->json(array('succes'=>true), 200);
            }
            $error="Vous être maintenant bien inscrit avec le pseudo : ".$request->pseudo;
            return view('index')->with("error", $error);
        } catch (Exception $e) {
            if ($request->wantsJson()) {
                return response()->json(array('succes'=>false, 'message'=>$e->getMessage()), 500);
            }
            return view('index')->with("error", $e->getMessage());
        }
    }

    /**
     * Connexion de l'utilisateur
     * @param Request $request Requete HTTP correspondante à la connexion
     * @return Response
     */
    public function connexion(Request $request)
    {
        if (!isset($error)) {
            $userdata = array(
                'pseudo'     => $request->get('pseudo'),
                'password'  => $request->get('password'),
            );
            // Tentative de connexion
            if (Auth::attempt($userdata)) {
                if ($request->wantsJson()) {
                    return response()->json(array('succes'=>true,
                        'admin'=>Auth::user()->isAdmin()), 200);
                }

                if (Auth::user()->isAdmin()) {
                    return redirect()->route('admin');
                } else {
                    return redirect()->route('user');
                }
            } else {
                if ($request->wantsJson()) {
                    return response()->json(array('succes' => false), 422);
                } else {
                    $wrong="Mauvais identifiants !";
                    return view('index')->with('wrong', $wrong);
                }
            }
        }
        return $error;
    }

    /**
     * Déconnexion de l'utilisateur
     * @param Request $request
     * @return string Contenu du log file
     */
    public function disconnect(Request $request)
    {
        Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        if ($request->wantsJson()) {
            return response()->json(array('succes'=>true), 200);
        }
        return redirect()->route('index');
    }

    /**
     * Renvoie la page de l'utilisateur
     * @return Application|Factory|View    Vue contenant la page de l'utilisateur
     * @throws Exception
     */
    public function getDashboardUser()
    {
        return view('user')->with("users", $this->user_repository->getAll());
    }

    /**
     * Renvoie la page de l'administrateur
     * @return Application|Factory|View    Vue contenant la page de l'administrateur
     * @throws Exception
     */
    public function getDashboardAdmin()
    {
        return view('admin')->with("users", $this->user_repository->getAll());
    }

    /**
     * Supprimer un utilisateur
     * @param UserDeleteRequest $request
     * @return string Contenu du log file
     * @throws Exception
     */
    public function deleteUser(UserDeleteRequest $request)
    {
        $is_deleted = false;
        if (!isset($error)) {
            $error=[];
            $user = $this->user_repository->getByPseudo($request->get('pseudo'));
            if ($user !== null) {
                $is_deleted = $this->user_repository->delete($user);
            } else {
                $error["notfound"] = "L'utilisateur ".$request->get('pseudo')." n'existe pas !";
            }
        }
        return view('admin')->with([
            "users" => $this->user_repository->getAll(),
            "isDeleted" => $is_deleted,
            "name_deleted" => $request->get('pseudo'),
            "wrong" => $error
        ]);
    }

    /**
     * Conversion d'un string vers un int
     * @param string $string_to_convert String à convertir
     * @return int associé au string
     */
    private function stoi(string $string_to_convert)
    {
        if (is_int($string_to_convert)||is_numeric($string_to_convert)) {
            $int=intval($string_to_convert);
            if ($int!==0||$string_to_convert==="0") {
                return $int;
            }
        }
        return 0;
    }
}
