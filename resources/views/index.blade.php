<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>Site de chatons trop meugnons</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
                margin-top: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Forum des passionnés de chat
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <fieldset>
                                <div class="card-header">
                                    <legend>Inscription</legend>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('inscription') }}" method="POST" >
                                            Nom<br>
                                            <input type="text" name="nom" placeholder="Nom" class="form-control" required="required"/>
                                            Prénom<br>
                                            <input type="text" name="prenom" placeholder="Prénom" class="form-control" required="required"/>
                                            Age<br>
                                            <input type="text" name="age" placeholder="Age" class="form-control" required="required"/>
                                            Pseudo<br>
                                            <input type="text" name="pseudo" placeholder="Pseudo" class="form-control" required="required"/>
                                            Adresse mail<br>
                                            <input type="email" name="email" placeholder="Mail" class="form-control" required="required"/>
                                            Mot de passe<br>
                                            <input type="password" name="password" placeholder="Mot de passe" class="form-control" required="required"/>
                                            @csrf
                                            <br>
                                            @if(isset($error))
                                                <small style="color:red;">{{$error}}</small><br/><br/>
                                            @endif
                                            <input type="submit" value="Envoyer" class="btn btn-dark"/>
                                    </form>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <fieldset>
                                <div class="card-header">
                                    <legend>Connexion</legend>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('connexion') }}" method="POST" >
                                            Pseudonyme<br>
                                            <input type="text" name="pseudo" class="form-control" required="required"/>
                                            Mot de passe<br>
                                            <input type="password" name="password" class="form-control" required="required"/><br>
                                            @csrf
                                            @if(isset($wrong))
                                                <small style="color:red;">{{$wrong}}</small><br/><br/>
                                            @endif
                                            <input type="submit" value="Envoyer" class="btn btn-dark"/>
                                    </form>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>
