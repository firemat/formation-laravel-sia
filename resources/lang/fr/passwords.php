<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit avoir au moins six caractères et doit être identique à la confirmation.',
    'reset' => 'Votre mot de passe a bien été mis à jour!',
    'throttled' => 'Veuillez attendre avant de réessayer.',
    'sent' => 'Nous vous avons envoyé un mail pour réinitialiser votre mot de passe!',
    'token' => 'Ce jeton est invalide ou a expiré, veuillez redemander un jeton.',
    'user' => "Nous ne trouvons pas d'utilisateur correspondant à votre adresse mail.",

];
