# Formation SIA Laravel changelog

The latest version of this file can be found at the master branch of the Formation SIA Laravel repository.

## 1.0.0

### Removed (0 change)

### Fixed (0 change, 0 of them are from the community)

### Changed (0 change)

### Added (3 changes)

- Fork Portail VA 2 Laravel server
- Adding messy solution for Blade view
- Adding SonarQube

### Other (0 change)
